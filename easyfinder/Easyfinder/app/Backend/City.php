<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
     protected $fillable = ['name','bangla_name','latitude','longitude'];
}
