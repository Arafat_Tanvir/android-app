<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
      protected $fillable = ['name','parent_id','description','image'];

      public function parent()
      {
        return $this->belongsTo(Category::class,'parent_id');
      }
}
