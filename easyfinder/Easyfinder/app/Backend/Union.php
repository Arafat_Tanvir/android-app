<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class Union extends Model
{
  protected $fillable = ['name','bangla_name','latitude','longitude','upazila_id'];

  public function upazila(){
    return $this->belongsTo(Upazila::class);
  }
}
