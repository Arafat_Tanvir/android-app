<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\Upazila;
use App\Backend\District;

class UpazilaController extends Controller
{
      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function index()
      {
          $upazilas =Upazila::orderBy('id','desc')->get();
          return view('Backend.admin.upazilas.index',compact('upazilas'));
      }

      /**
       * Show the form for creating a new resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function create()
      {
          $districts =District::orderBy('id','desc')->get();
          return view('Backend.admin.upazilas.create',compact('districts'));
      }

      /**
       * Store a newly created resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       */
      public function store(Request $request)
      {
          $this->validate($request,[
            'name'=>'required|max:50|min:5',
          ]);

          $upazilas=new Upazila();
          $upazilas->name=$request->name;
          $upazilas->bangla_name=$request->bangla_name;
          $upazilas->latitude=$request->latitude;
          $upazilas->longitude=$request->longitude;
          $upazilas->district_id=$request->district_id;
          //dd($upazilas);
          $upazilas->save();
          if(!is_null($upazilas)){
            session()->flash('success','Upazila create Successfully!!');
            return redirect()->route('upazilas.index');
          }else{
            session()->flash('success','Some Error Occer!!');
            return back();
          }

      }

      /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function show($id)
      {
           $upazilas=Upazila::findOrFail($id);
           //dd($upazilas);
           return view('Backend.admin.upazilas.show',compact('upazilas'));
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function edit($id)
      {
          $districts =District::orderBy('id','desc')->get();
          $upazilas = Upazila::findOrFail($id);
          return view('Backend.admin.upazilas.edit',compact('districts','upazilas'));
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function update(Request $request, $id)
      {
            //dd($id);
            $this->validate($request,[
              'name'=>'required|max:50|min:5',
              'district_id'=>'required',
            ]);

            $upazilas=Upazila::findOrFail($id);
            $upazilas->name=$request->name;
            $upazilas->bangla_name=$request->bangla_name;
            $upazilas->latitude=$request->latitude;
            $upazilas->longitude=$request->longitude;
            $upazilas->district_id=$request->district_id;
            $upazilas->update();
            if(!is_null($upazilas)){
                session()->flash('success','Upazila Update Successfully!!');
                return redirect()->route('upazilas.index');
            }else{
                session()->flash('sticky_error','Some Error Occer!!');
                return back();
            }
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function destroy($id)
      {
          $upazilas=Upazila::find($id);
          if(!is_null($upazilas))
          {
              $upazilas->delete();
              session()->flash('success','Product has delete Successfully');
              return back();
          }else{
              session()->flash('sticky_error','Some Error Occer');
              return back();
          }

      }
}
