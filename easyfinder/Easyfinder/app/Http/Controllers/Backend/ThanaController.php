<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\Thana;
use App\Backend\City;

class ThanaController extends Controller
{
      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function index()
      {
          $thanas =Thana::orderBy('id','desc')->get();
          return view('Backend.admin.thanas.index',compact('thanas'));
      }

      /**
       * Show the form for creating a new resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function create()
      {
          $cities =City::orderBy('id','desc')->get();
          return view('Backend.admin.thanas.create',compact('cities'));
      }

      /**
       * Store a newly created resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       */
      public function store(Request $request)
      {
          $this->validate($request,[
            'name'=>'required|max:50|min:5',
          ]);

          $thanas=new Thana();
          $thanas->name=$request->name;
          $thanas->bangla_name=$request->bangla_name;
          $thanas->latitude=$request->latitude;
          $thanas->longitude=$request->longitude;
          $thanas->city_id=$request->city_id;
          //dd($thanas);
          $thanas->save();
          if(!is_null($thanas)){
            session()->flash('success','Thana create Successfully!!');
            return redirect()->route('thanas.index');
          }else{
            session()->flash('success','Some Error Occer!!');
            return back();
          }

      }

      /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function show($id)
      {
           $thanas=Thana::findOrFail($id);
           //dd($thanas);
           return view('Backend.admin.thanas.show',compact('thanas'));
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function edit($id)
      {
          $cities =City::orderBy('id','desc')->get();
          $thanas = Thana::findOrFail($id);
          return view('Backend.admin.thanas.edit',compact('cities','thanas'));
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function update(Request $request, $id)
      {
            //dd($id);
            $this->validate($request,[
              'name'=>'required|max:50|min:5',
              'city_id'=>'required',
            ]);

            $thanas=Thana::findOrFail($id);
            $thanas->name=$request->name;
            $thanas->bangla_name=$request->bangla_name;
            $thanas->latitude=$request->latitude;
            $thanas->longitude=$request->longitude;
            $thanas->city_id=$request->city_id;
            $thanas->update();
            if(!is_null($thanas)){
                session()->flash('success','Thana Update Successfully!!');
                return redirect()->route('thanas.index');
            }else{
                session()->flash('sticky_error','Some Error Occer!!');
                return back();
            }
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function destroy($id)
      {
          $thanas=Thana::find($id);
          if(!is_null($thanas))
          {
              $thanas->delete();
              session()->flash('success','Product has delete Successfully');
              return back();
          }else{
              session()->flash('sticky_error','Some Error Occer');
              return back();
          }

      }
}
