<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\Category;
use Image;
use File;

class CategoryController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $categories =Category::orderBy('id','desc')->get();
      return view('Backend.admin.categories.index',compact('categories'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      $categories = Category::orderBy('name','desc')->where('parent_id',NULL)->get();
      return view('Backend.admin.categories.create',compact('categories'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $this->validate($request,[
        'name'=>'required|max:50|min:5',
      ]);

      $categories=new Category();
      $categories->name=$request->name;
      $categories->parent_id=$request->parent_id;
      $categories->description=$request->description;
      // if(count($request->image)>0)
      //   {
      //   	  $image=$request->file('image');
      //       $img=time().'.'.$image->getClientOriginalExtension();
      //       $location=public_path('image/categories/'.$img);
      //       Image::make($image)->save($location)->resize(300,300);
      //       $categories->image=$img;
      //   }
      //dd($categories);
      $categories->save();
      if(!is_null($categories)){
        session()->flash('success','Category create Successfully!!');
        return redirect()->route('categories.index');
      }else{
        session()->flash('success','Some Error Occer!!');
        return back();
      }

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
       $categories=Category::findOrFail($id);
       //dd($categories);
       return view('Backend.admin.categories.show',compact('categories'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      $main_categories = Category::orderBy('name','desc')->where('parent_id',NULL)->get();
    	$categories = Category::findOrFail($id);
      if(!is_null($categories)){
          return view('Backend.admin.categories.edit',compact('categories','main_categories'));
      }else{
          return redirect()->route('categories');
      }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
        //dd($id);
        $this->validate($request,[
          'name'=>'required|max:50|min:5',
          'parent_id'=>'required',
        ]);

        $categories=Category::findOrFail($id);
        $categories->name=$request->name;
        $categories->parent_id=$request->parent_id;
        $categories->description=$request->description;
        //dd($categories);
        $categories->update();
        if(!is_null($categories)){
            session()->flash('success','Category Update Successfully!!');
            return redirect()->route('categories.index');
        }else{
            session()->flash('sticky_error','Some Error Occer!!');
            return back();
        }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function delete($id)
  {
      $categories=Category::find($id);
      if(!is_null($categories))
      {
          $categories->delete();
          session()->flash('success','Product has delete Successfully');
          return back();
      }else{
          session()->flash('sticky_error','Some Error Occer');
          return back();
      }

  }
}
