<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\Division;

class DivisionController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $divisions =Division::orderBy('id','desc')->get();
      return view('Backend.admin.divisions.index',compact('divisions'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view('Backend.admin.divisions.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $this->validate($request,[
        'name'=>'required|max:50|min:5',
      ]);

      $divisions=new Division();
      $divisions->name=$request->name;
      $divisions->bangla_name=$request->bangla_name;
      $divisions->latitude=$request->latitude;
      $divisions->longitude=$request->longitude;
      $divisions->save();
      if(!is_null($divisions)){
        session()->flash('success','Division create Successfully!!');
        return redirect()->route('divisions.index');
      }else{
        session()->flash('success','Some Error Occer!!');
        return back();
      }

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
       $divisions=Division::findOrFail($id);
       return view('Backend.admin.divisions.show',compact('divisions'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      $divisions = Division::findOrFail($id);
      if(!is_null($divisions)){
          return view('Backend.admin.divisions.edit',compact('divisions'));
      }else{
          return back();
      }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
        //dd($id);
        $this->validate($request,[
          'name'=>'required|max:50|min:5',
        ]);

        $divisions=Division::findOrFail($id);
        $divisions->name=$request->name;
        $divisions->bangla_name=$request->bangla_name;
        $divisions->latitude=$request->latitude;
        $divisions->longitude=$request->longitude;
        $divisions->update();
        if(!is_null($divisions)){
            session()->flash('success','Division Update Successfully!!');
            return redirect()->route('divisions.index');
        }else{
            session()->flash('sticky_error','Some Error Occer!!');
            return back();
        }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      $divisions=Division::find($id);
      if(!is_null($divisions))
      {
          $divisions->delete();
          session()->flash('success','Product has delete Successfully');
          return back();
      }else{
          session()->flash('sticky_error','Some Error Occer');
          return back();
      }

  }
}
