<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\Bechelor;

class BechelorController extends Controller
{
      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function index()
      {
        //  $bechelors =Bechelor::orderBy('id','desc')->get();
          return view('Backend.admin.bechelors.index',compact('bechelors'));
      }

      /**
       * Show the form for creating a new resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function create()
      {
          $bechelors = Bechelor::orderBy('name','desc')->where('parent_id',NULL)->get();
          return view('Backend.admin.bechelors.create',compact('bechelors'));
      }

      /**
       * Store a newly created resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       */
      public function store(Request $request)
      {
          $this->validate($request,[
            'name'=>'required|max:50|min:5',
          ]);

          $bechelors=new Bechelor();
          $bechelors->name=$request->name;
          $bechelors->parent_id=$request->parent_id;
          $bechelors->description=$request->description;
          // if(count($request->image)>0)
          //   {
          //   	  $image=$request->file('image');
          //       $img=time().'.'.$image->getClientOriginalExtension();
          //       $location=public_path('image/bechelors/'.$img);
          //       Image::make($image)->save($location)->resize(300,300);
          //       $bechelors->image=$img;
          //   }
          //dd($bechelors);
          $bechelors->save();
          if(!is_null($bechelors)){
            session()->flash('success','Bechelor create Successfully!!');
            return redirect()->route('bechelors.index');
          }else{
            session()->flash('success','Some Error Occer!!');
            return back();
          }

      }

      /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function show($id)
      {
           $bechelors=Bechelor::findOrFail($id);
           //dd($bechelors);
           return view('Backend.admin.bechelors.show',compact('bechelors'));
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function edit($id)
      {
          $main_bechelors = Bechelor::orderBy('name','desc')->where('parent_id',NULL)->get();
          $bechelors = Bechelor::findOrFail($id);
          if(!is_null($bechelors)){
              return view('Backend.admin.bechelors.edit',compact('bechelors','main_bechelors'));
          }else{
              return redirect()->route('bechelors');
          }
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function update(Request $request, $id)
      {
            //dd($id);
            $this->validate($request,[
              'name'=>'required|max:50|min:5',
              'parent_id'=>'required',
            ]);

            $bechelors=Bechelor::findOrFail($id);
            $bechelors->name=$request->name;
            $bechelors->parent_id=$request->parent_id;
            $bechelors->description=$request->description;
            //dd($bechelors);
            $bechelors->update();
            if(!is_null($bechelors)){
                session()->flash('success','Bechelor Update Successfully!!');
                return redirect()->route('bechelors.index');
            }else{
                session()->flash('sticky_error','Some Error Occer!!');
                return back();
            }
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function delete($id)
      {
          $bechelors=Bechelor::find($id);
          if(!is_null($bechelors))
          {
              $bechelors->delete();
              session()->flash('success','Product has delete Successfully');
              return back();
          }else{
              session()->flash('sticky_error','Some Error Occer');
              return back();
          }

      }
}
