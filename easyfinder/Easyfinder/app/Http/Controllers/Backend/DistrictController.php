<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\Division;
use App\Backend\District;

class DistrictController extends Controller
{
      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function index()
      {
          $districts =District::orderBy('id','desc')->get();
          return view('Backend.admin.districts.index',compact('districts'));
      }

      /**
       * Show the form for creating a new resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function create()
      {
          $divisions =Division::orderBy('id','desc')->get();
          return view('Backend.admin.districts.create',compact('divisions'));
      }

      /**
       * Store a newly created resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       */
      public function store(Request $request)
      {
          $this->validate($request,[
            'name'=>'required|max:50|min:5',
          ]);

          $districts=new District();
          $districts->name=$request->name;
          $districts->bangla_name=$request->bangla_name;
          $districts->latitude=$request->latitude;
          $districts->longitude=$request->longitude;
          $districts->division_id=$request->division_id;
          //dd($districts);
          $districts->save();
          if(!is_null($districts)){
            session()->flash('success','District create Successfully!!');
            return redirect()->route('districts.index');
          }else{
            session()->flash('success','Some Error Occer!!');
            return back();
          }

      }

      /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function show($id)
      {
           $districts=District::findOrFail($id);
           //dd($districts);
           return view('Backend.admin.districts.show',compact('districts'));
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function edit($id)
      {
          $divisions =Division::orderBy('id','desc')->get();
          $districts = District::findOrFail($id);
          return view('Backend.admin.districts.edit',compact('divisions','districts'));
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function update(Request $request, $id)
      {
            //dd($id);
            $this->validate($request,[
              'name'=>'required|max:50|min:5',
              'division_id'=>'required',
            ]);

            $districts=District::findOrFail($id);
            $districts->name=$request->name;
            $districts->bangla_name=$request->bangla_name;
            $districts->latitude=$request->latitude;
            $districts->longitude=$request->longitude;
            $districts->division_id=$request->division_id;
            $districts->update();
            if(!is_null($districts)){
                session()->flash('success','District Update Successfully!!');
                return redirect()->route('districts.index');
            }else{
                session()->flash('sticky_error','Some Error Occer!!');
                return back();
            }
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function destroy($id)
      {
          $districts=District::find($id);
          if(!is_null($districts))
          {
              $districts->delete();
              session()->flash('success','Product has delete Successfully');
              return back();
          }else{
              session()->flash('sticky_error','Some Error Occer');
              return back();
          }

      }
}
