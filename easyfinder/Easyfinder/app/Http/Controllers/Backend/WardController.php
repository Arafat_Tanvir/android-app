<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\Ward;
use App\Backend\Thana;

class WardController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $wards =Ward::orderBy('id','desc')->get();
      return view('Backend.admin.wards.index',compact('wards'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      $thanas =Thana::orderBy('id','desc')->get();
      return view('Backend.admin.wards.create',compact('thanas'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $this->validate($request,[
        'name'=>'required|max:50|min:5',
      ]);

      $wards=new Ward();
      $wards->name=$request->name;
      $wards->bangla_name=$request->bangla_name;
      $wards->latitude=$request->latitude;
      $wards->longitude=$request->longitude;
      $wards->thana_id=$request->thana_id;
      //dd($wards);
      $wards->save();
      if(!is_null($wards)){
        session()->flash('success','Ward create Successfully!!');
        return redirect()->route('wards.index');
      }else{
        session()->flash('success','Some Error Occer!!');
        return back();
      }

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
       $wards=Ward::findOrFail($id);
       //dd($wards);
       return view('Backend.admin.wards.show',compact('wards'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      $thanas =Thana::orderBy('id','desc')->get();
      $wards = Ward::findOrFail($id);
      return view('Backend.admin.wards.edit',compact('thanas','wards'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
        //dd($id);
        $this->validate($request,[
          'name'=>'required|max:50|min:5',
          'thana_id'=>'required',
        ]);

        $wards=Ward::findOrFail($id);
        $wards->name=$request->name;
        $wards->bangla_name=$request->bangla_name;
        $wards->latitude=$request->latitude;
        $wards->longitude=$request->longitude;
        $wards->thana_id=$request->thana_id;
        $wards->update();
        if(!is_null($wards)){
            session()->flash('success','Ward Update Successfully!!');
            return redirect()->route('wards.index');
        }else{
            session()->flash('sticky_error','Some Error Occer!!');
            return back();
        }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      $wards=Ward::find($id);
      if(!is_null($wards))
      {
          $wards->delete();
          session()->flash('success','Product has delete Successfully');
          return back();
      }else{
          session()->flash('sticky_error','Some Error Occer');
          return back();
      }

  }
}
