<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\Union;
use App\Backend\Upazila;

class UnionController extends Controller
{
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $unions =Union::orderBy('id','desc')->get();
            return view('Backend.admin.unions.index',compact('unions'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $upazilas =Upazila::orderBy('id','desc')->get();
            return view('Backend.admin.unions.create',compact('upazilas'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $this->validate($request,[
              'name'=>'required|max:50|min:5',
            ]);

            $unions=new Union();
            $unions->name=$request->name;
            $unions->bangla_name=$request->bangla_name;
            $unions->latitude=$request->latitude;
            $unions->longitude=$request->longitude;
            $unions->upazila_id=$request->upazila_id;
            //dd($unions);
            $unions->save();
            if(!is_null($unions)){
              session()->flash('success','Union create Successfully!!');
              return redirect()->route('unions.index');
            }else{
              session()->flash('success','Some Error Occer!!');
              return back();
            }

        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
             $unions=Union::findOrFail($id);
             //dd($unions);
             return view('Backend.admin.unions.show',compact('unions'));
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $upazilas =Upazila::orderBy('id','desc')->get();
            $unions = Union::findOrFail($id);
            return view('Backend.admin.unions.edit',compact('upazilas','unions'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
              //dd($id);
              $this->validate($request,[
                'name'=>'required|max:50|min:5',
                'upazila_id'=>'required',
              ]);

              $unions=Union::findOrFail($id);
              $unions->name=$request->name;
              $unions->bangla_name=$request->bangla_name;
              $unions->latitude=$request->latitude;
              $unions->longitude=$request->longitude;
              $unions->upazila_id=$request->upazila_id;
              $unions->update();
              if(!is_null($unions)){
                  session()->flash('success','Union Update Successfully!!');
                  return redirect()->route('unions.index');
              }else{
                  session()->flash('sticky_error','Some Error Occer!!');
                  return back();
              }
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $unions=Union::find($id);
            if(!is_null($unions))
            {
                $unions->delete();
                session()->flash('success','Product has delete Successfully');
                return back();
            }else{
                session()->flash('sticky_error','Some Error Occer');
                return back();
            }

        }
}
