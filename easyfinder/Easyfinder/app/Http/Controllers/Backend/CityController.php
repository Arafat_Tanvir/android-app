<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\City;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities =City::orderBy('id','desc')->get();
        return view('Backend.admin.cities.index',compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Backend.admin.cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          'name'=>'required|max:50|min:5',
        ]);

        $cities=new City();
        $cities->name=$request->name;
        $cities->bangla_name=$request->bangla_name;
        $cities->latitude=$request->latitude;
        $cities->longitude=$request->longitude;
        $cities->save();
        if(!is_null($cities)){
          session()->flash('success','City create Successfully!!');
          return redirect()->route('cities.index');
        }else{
          session()->flash('success','Some Error Occer!!');
          return back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $cities=City::findOrFail($id);
         return view('Backend.admin.cities.show',compact('cities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cities = City::findOrFail($id);
        if(!is_null($cities)){
            return view('Backend.admin.cities.edit',compact('cities'));
        }else{
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          //dd($id);
          $this->validate($request,[
            'name'=>'required|max:50|min:5',
          ]);

          $cities=City::findOrFail($id);
          $cities->name=$request->name;
          $cities->bangla_name=$request->bangla_name;
          $cities->latitude=$request->latitude;
          $cities->longitude=$request->longitude;
          $cities->update();
          if(!is_null($cities)){
              session()->flash('success','City Update Successfully!!');
              return redirect()->route('cities.index');
          }else{
              session()->flash('sticky_error','Some Error Occer!!');
              return back();
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cities=City::find($id);
        if(!is_null($cities))
        {
            $cities->delete();
            session()->flash('success','Product has delete Successfully');
            return back();
        }else{
            session()->flash('sticky_error','Some Error Occer');
            return back();
        }

    }
}
