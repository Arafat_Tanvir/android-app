<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('divisions','Backend\DivisionController');
Route::resource('districts','Backend\DistrictController');
Route::resource('upazilas','Backend\UpazilaController');
Route::resource('unions','Backend\UnionController');
Route::resource('cities','Backend\CityController');
Route::resource('thanas','Backend\ThanaController');
Route::resource('wards','Backend\WardController');

Route::group(['prefix'=>'categories'],function(){

    Route::get('/',"Backend\CategoryController@index")->name('categories.index');
    Route::get('/create',"Backend\CategoryController@create")->name('categories.create');
    Route::post('/store',"Backend\CategoryController@store")->name('categories.store');
    Route::get('/show/{id}',"Backend\CategoryController@show")->name('categories.show');
    Route::get('/edit/{id}',"Backend\CategoryController@edit")->name('categories.edit');
    Route::post('/update/{id}',"Backend\CategoryController@update")->name('categories.update');
    Route::post('/delete/{id}',"Backend\CategoryController@delete")->name('categories.delete');

});
Route::group(['prefix'=>'/bechelors'],function(){

//bechelors route details
Route::get('/',"Backend\BechelorController@index")->name('bechelors.index');
Route::get('/create',"Backend\BechelorController@create")->name('bechelors.create');
Route::post('/store',"Backend\BechelorController@store")->name('bechelors.store');
Route::get('/show/{id}',"Backend\BechelorController@show")->name('bechelors.show');
Route::get('/edit/{id}',"Backend\BechelorController@edit")->name('bechelors.edit');
Route::post('/update/{id}',"Backend\BechelorController@update")->name('bechelors.update');
Route::post('/delete/{id}',"Backend\BechelorController@delete")->name('bechelors.delete');

});
// Route::group(['prefix'=>'/divisions'],function(){
//
// //Division route details
// Route::get('/',"DivisionController@index")->name('divisions.index');
// Route::get('/create',"DivisionController@create")->name('divisions.create');
// Route::post('/store',"DivisionController@store")->name('divisions.store');
// Route::get('/show/{id}',"DivisionController@show")->name('divisions.show');
// Route::get('/edit/{id}',"DivisionController@edit")->name('divisions.edit');
// Route::post('/update/{id}',"DivisionController@update")->name('divisions.update');
// Route::post('/delete/{id}',"DivisionController@delete")->name('divisions.delete');
//
// });
// Route::group(['prefix'=>'/divisions'],function(){
//
// //Division route details
// Route::get('/',"DivisionController@index")->name('divisions.index');
// Route::get('/create',"DivisionController@create")->name('divisions.create');
// Route::post('/store',"DivisionController@store")->name('divisions.store');
// Route::get('/show/{id}',"DivisionController@show")->name('divisions.show');
// Route::get('/edit/{id}',"DivisionController@edit")->name('divisions.edit');
// Route::post('/update/{id}',"DivisionController@update")->name('divisions.update');
// Route::post('/delete/{id}',"DivisionController@delete")->name('divisions.delete');
//
// });
// Route::group(['prefix'=>'/divisions'],function(){
//
// //Division route details
// Route::get('/',"DivisionController@index")->name('divisions.index');
// Route::get('/create',"DivisionController@create")->name('divisions.create');
// Route::post('/store',"DivisionController@store")->name('divisions.store');
// Route::get('/show/{id}',"DivisionController@show")->name('divisions.show');
// Route::get('/edit/{id}',"DivisionController@edit")->name('divisions.edit');
// Route::post('/update/{id}',"DivisionController@update")->name('divisions.update');
// Route::post('/delete/{id}',"DivisionController@delete")->name('divisions.delete');
//
// });
// Route::group(['prefix'=>'/divisions'],function(){
//
// //Division route details
// Route::get('/',"DivisionController@index")->name('divisions.index');
// Route::get('/create',"DivisionController@create")->name('divisions.create');
// Route::post('/store',"DivisionController@store")->name('divisions.store');
// Route::get('/show/{id}',"DivisionController@show")->name('divisions.show');
// Route::get('/edit/{id}',"DivisionController@edit")->name('divisions.edit');
// Route::post('/update/{id}',"DivisionController@update")->name('divisions.update');
// Route::post('/delete/{id}',"DivisionController@delete")->name('divisions.delete');
//
// });
// Route::group(['prefix'=>'/divisions'],function(){
//
// //Division route details
// Route::get('/',"DivisionController@index")->name('divisions.index');
// Route::get('/create',"DivisionController@create")->name('divisions.create');
// Route::post('/store',"DivisionController@store")->name('divisions.store');
// Route::get('/show/{id}',"DivisionController@show")->name('divisions.show');
// Route::get('/edit/{id}',"DivisionController@edit")->name('divisions.edit');
// Route::post('/update/{id}',"DivisionController@update")->name('divisions.update');
// Route::post('/delete/{id}',"DivisionController@delete")->name('divisions.delete');
//
// });
