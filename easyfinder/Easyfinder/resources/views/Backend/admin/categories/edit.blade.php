@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <div class="row">
        <div class="col-sm-8">
          <div class="card">
                <div class="card-header">
                  <h2>Category Edit <span class="pull-right"><a href="{{ route('categories.index')}}">Back</a></span></h2>
                </div>
                 <div class="card-body">
                   <form method="post" action="{{ route('categories.update',$categories->id) }}" enctype="multipart/form-data" >
                       @csrf
                       <div class="form-group">
                           <label for="name">Category Name</label>
                           <div class="form-input">
                               <input type="text" class="form-control is-invalid form-control-sm" name="name" id="name" value="{{ $categories->name}}">
                               <div class="invalid-feedback">
                                 {{ ($errors->has('name')) ? $errors->first('name') : ''}}
                               </div>
                           </div>
                       </div>

                       <div class="form-group">
                           <label for="parent_id">Primary Name Off Category</label>
                           <div class="form-input">
                               <select name="parent_id" id="parent_id" class="form-control is-valid form-control-sm input-md">
                                       <option value="0" disabled="true" selected="true">===Select Parent Category===</option>
                                       @foreach($main_categories as $category)
                                       <option value="{{$category->id}}" {{ $category->id == $categories->parent_id ? 'selected' : ''}}>{{$category->name}}</option>
                                       @endforeach
                               </select>
                               <div class="valid-feedback">
                                 {{ ($errors->has('parent_id')) ? $errors->first('parent_id') : ''}}
                               </div>
                           </div>
                       </div>


                        <div class="form-group">
                            <label for="description">Description</label>
                            <div class="form-input">
                                <textarea name="description" cols="4" rows="5" class="form-control is-valid form-control-sm input-md" id="description">{{ $categories->description }}</textarea>
                                <div class="valid-feedback">
                                  {{ ($errors->has('description')) ? $errors->first('description') : ''}}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="image">Category Image</label>
                            <div class="form-input">
                                <input type="file" class="form-control is-valid form-control-sm input-md" name="image" id="image" value="{{ old('image')}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('image')) ? $errors->first('image') : ''}}
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary" type="submit">Update</button>
                    </form>
                  </div>
                </div>
              </div>
        <div class="col-sm-4">
          <div id="map">

  </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
