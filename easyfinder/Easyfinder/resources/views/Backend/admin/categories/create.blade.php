@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
 		<div class="row ">
 			<div class="col-xs-8 col-sm-8 col-md-8 offset-xs-2 offset-sm-2 offset-md-2">
      <div class="card">
        <div class="card-header">
          <h2>Categories Create Form <span class="pull-right"><a href="{{ route('categories.index')}}">Back</a></span></h2>
        </div>
        <div class="card-body">
      <form method="POST" action="{{ route('categories.store') }}" enctype="multipart/form-data">
          @csrf

          <div class="form-group">
              <label for="name">Name</label>
              <div class="form-input">
                  <input type="text" class="form-control is-invalid form-control-sm" name="name" id="name" placeholder="Enter Category name" value="{{old('name')}}" required>
                  <div class="invalid-feedback">
                    {{ ($errors->has('name')) ? $errors->first('name') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group">
              <label for="parent_id">Select Parent Category</label>
              <div class="form-input">
                  <select name="parent_id" id="parent_id" class="form-control is-valid form-control-sm input-md" value="">
                          <option value="0" disabled="true" selected="true">===Select Parent Category for this Category===</option>
                          @foreach($categories as $category)
                          <option value="{{$category->id}}">{{$category->name}}</option>
                          @endforeach
                  </select>
                  <div class="valid-feedback">
                    {{ ($errors->has('parent_id')) ? $errors->first('parent_id') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group">
              <label for="description">Description</label>
              <div class="form-input">
                  <textarea name="description" cols="4" rows="5" value="{{old('description')}}" class="form-control is-valid form-control-sm input-md" id="description"required>{{ old('description')}}</textarea>
                  <div class="valid-feedback">
                    {{ ($errors->has('description')) ? $errors->first('description') : ''}}
                  </div>
              </div>
          </div>


          <div class="form-group">
              <label for="image">Category Image</label>
              <div class="form-input">
                  <input type="file" class="form-control is-valid form-control-sm" name="image" id="image" placeholder="Enter Ward Bangla Name" value="">
                  <div class="valid-feedback">
                    {{ ($errors->has('image')) ? $errors->first('image') : ''}}
                  </div>
              </div>
          </div>


          <button class="btn btn-primary" type="submit">Create</button>
      </form>
    </div>
  </div>
</div>

</div>
</div>
@endsection
