@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <div class="card-header mt-2">
        <h2>Category List<span class="pull-right"><a href="{{ route('categories.create')}}">Create</a></span></h2>
      </div>
      <div class="table-responsive mt-2">
        <table id="categories" class="table table-bordered table-striped">
          <caption>List of categories</caption>
          <thead>
  					<tr>
  						<th>SL</th>
  						<th>Name</th>
  						<th>Description</th>
  						<th>Image</th>
              <th>Parent Of the Category</th>
              <th>Show</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($categories as $category)
  						<td>{{ $a++ }}</td>
  						<td>{{ $category->name }}</td>
  						<td>
  						    @if($category->description)
                  <p>{{ $category->description}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
  						    @if($category->image)
                  <p>{{ $category->image}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td style="color: red">
							@if($category->parent_id==null)
							Primary Category
							@else
							{{ $category->parent->name }}
							@endif
						</td>
              <td>
                <a href="{{route('categories.show', $category->id)}}" class="badge badge-primary">Show</a>
              </td>
  						<td>
  							<a href="{{route('categories.edit', $category->id)}}" class="badge badge-warning">Edit</a>
                <a href="#DeleteModal{{ $category->id}}" data-toggle="modal" class="badge badge-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('categories.delete', $category->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="badge badge-success">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
	$(document).ready(function() {
    $('#categories').DataTable();
} );
</script>
@endsection
