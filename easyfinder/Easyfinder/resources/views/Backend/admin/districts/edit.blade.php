@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <div class="row">
        <div class="col-sm-8">
          <div class="card">
                <div class="card-header">
                  <h2>Dirstrict Edit <span class="pull-right"><a href="{{ route('districts.index')}}">Back</a></span></h2>
                </div>
                 <div class="card-body">
                   <form method="POST" action="{{ route('districts.update',$districts->id) }}" >
                       @csrf
                       @method('PUT')

                       <div class="form-group">
                           <label for="latitude">Division Name</label>
                           <div class="form-input">
                               <select name="division_id" class="form-control is-valid form-control-sm input-md" value="{{old('city_id')}}">
                                       <option value="0" disabled="true" selected="true">===Select Under City===</option>
                                       @foreach($divisions as $division)
                                       <option value="{{$division->id}}" {{ $division->id == $districts->division_id ? 'selected' : ''}}>{{$division->name}}</option>
                                       @endforeach
                               </select>
                               <div class="valid-feedback">
                                 {{ ($errors->has('division_id')) ? $errors->first('division_id') : ''}}
                               </div>
                           </div>
                       </div>


                        <div class="form-group">
                            <label for="name">Dirstrict Name</label>
                            <div class="form-input">
                                <input type="text" class="form-control is-invalid form-control-sm" name="name" id="name" value="{{ $districts->name}}">
                                <div class="invalid-feedback">
                                  {{ ($errors->has('name')) ? $errors->first('name') : ''}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bangla_name">Dirstrict Bangla Name</label>
                            <div class="form-input">
                                <input type="text" class="form-control is-valid form-control-sm" name="bangla_name" id="bangla_name" value="{{ $districts->bangla_name? $districts->bangla_name:'N\A'}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('bangla_name')) ? $errors->first('bangla_name') : ''}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="latitude">Dirstrict Latitude</label>
                            <div class="form-input">
                                <input type="number" class="form-control is-valid form-control-sm input-md" step="0.01" name="latitude" id="latitude" value="{{ $districts->latitude? $districts->latitude:'0.00'}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('latitude')) ? $errors->first('latitude') : ''}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="longitude">Dirstrict Longitude</label>
                            <div class="form-input">
                                <input type="number" class="form-control is-valid form-control-sm input-md"step="0.01" name="longitude" id="longitude" value="{{ $districts->longitude? $districts->longitude:'0.00'}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('longitude')) ? $errors->first('longitude') : ''}}
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Update</button>
                    </form>
                  </div>
                </div>
              </div>
        <div class="col-sm-4">
          <div id="map">

  </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
