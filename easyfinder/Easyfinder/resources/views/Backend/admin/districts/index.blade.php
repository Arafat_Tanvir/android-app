@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <div class="card-header mt-2">
        <h2>District List<span class="pull-right"><a href="{{ route('districts.create')}}">Create</a></span></h2>
      </div>
      <div class="table-responsive mt-2">
        <table id="districts" class="table table-bordered table-striped">
          <caption>List of districts</caption>
          <thead>
  					<tr>
  						<th>SL</th>
  						<th>Name</th>
  						<th>Bangla name</th>
  						<th>Latitude</th>
              <th>Longitude</th>
              <th>Parent District Name</th>
              <th>Show Details</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($districts as $district)
  						<td>{{ $a++ }}</td>
  						<td>{{ $district->name }}</td>
  						<td>
  						    @if($district->bangla_name)
                  <p>{{ $district->bangla_name}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
  						    @if($district->latitude)
                  <p>{{ $district->latitude}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
  						    @if($district->longitude)
                  <p>{{ $district->longitude}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td style="color:green">{{$district->division->name}}</td>
              <td>
                <a href="{{route('districts.show', $district->id)}}" class="badge badge-primary">Show</a>
              </td>
  						<td>
  							<a href="{{route('districts.edit', $district->id)}}" class="badge badge-warning">Edit</a>
                <a href="#" onclick="return confirm('are you sure')">
									<form class="pull-right form-inline" action="{{route('districts.destroy', $district->id)}}" method="POST">
										{{csrf_field()}}
										{{method_field('DELETE')}}
										<button class="badge badge-danger" type="submit">Delete</button>
									</form>
								</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
	$(document).ready(function() {
    $('#districts').DataTable();
} );
</script>
@endsection
