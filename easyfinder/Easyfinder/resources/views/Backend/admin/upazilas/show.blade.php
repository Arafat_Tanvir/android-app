@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <h4>Upazila Details<a href="{{route('upazilas.index')}}" class="btn btn-primary pull-right">Back</a></h4>
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-4">
                   <p>Upazila name</p>
                   <p>Upazila Bangla Name</p>
                   <p>Upazila Latitude </p>
                   <p>Longitude</p>
            </div>
            <div class="col-sm-8">
                   <p>Division: {{$upazilas->district->name}}, Upazila: {{$upazilas->name}}</p>
                   <p>{{$upazilas->bangla_name?$upazilas->bangla_name:'N\A'}} </p>
                   <p>{{$upazilas->latitude?$upazilas->latitude:'N\A'}} </p>
                   <p>{{$upazilas->longitude?$upazilas->longitude:'N\A'}} </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">

          <div id="map">

  </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
