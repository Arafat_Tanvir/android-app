@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <div class="card-header mt-2">
        <h2>Upazila List<span class="pull-right"><a href="{{ route('upazilas.create')}}">Create</a></span></h2>
      </div>
      <div class="table-responsive mt-2">
        <table id="upazilas" class="table table-bordered table-striped">
          <caption>List of upazilas</caption>
          <thead>
  					<tr>
  						<th>SL</th>
  						<th>Name</th>
  						<th>Bangla name</th>
  						<th>Latitude</th>
              <th>Longitude</th>
              <th>Parent Upazila Name</th>
              <th>Show Details</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($upazilas as $upazila)
  						<td>{{ $a++ }}</td>
  						<td>{{ $upazila->name }}</td>
  						<td>
  						    @if($upazila->bangla_name)
                  <p>{{ $upazila->bangla_name}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
  						    @if($upazila->latitude)
                  <p>{{ $upazila->latitude}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
  						    @if($upazila->longitude)
                  <p>{{ $upazila->longitude}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td style="color:green">{{$upazila->district->name}}</td>
              <td>
                <a href="{{route('upazilas.show', $upazila->id)}}" class="badge badge-primary">Show</a>
              </td>
  						<td>
  							<a href="{{route('upazilas.edit', $upazila->id)}}" class="badge badge-warning">Edit</a>
                <a href="#" onclick="return confirm('are you sure')">
									<form class="pull-right form-inline" action="{{route('upazilas.destroy', $upazila->id)}}" method="POST">
										{{csrf_field()}}
										{{method_field('DELETE')}}
										<button class="badge badge-danger" type="submit">Delete</button>
									</form>
								</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
	$(document).ready(function() {
    $('#upazilas').DataTable();
} );
</script>
@endsection
