@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <div class="row">
        <div class="col-sm-8">
          <div class="card">
                <div class="card-header">
                  <h2>Thana Edit <span class="pull-right"><a href="{{ route('thanas.index')}}">Back</a></span></h2>
                </div>
                 <div class="card-body">
                   <form method="POST" action="{{ route('thanas.update',$thanas->id) }}" >
                       @csrf
                       @method('PUT')

                       <div class="form-group">
                           <label for="latitude">City Name</label>
                           <div class="form-input">
                               <select name="city_id" class="form-control is-valid form-control-sm input-md" value="{{old('city_id')}}">
                                       <option value="0" disabled="true" selected="true">===Select Under City===</option>
                                       @foreach($cities as $city)
                                       <option value="{{$city->id}}" {{ $city->id == $thanas->city_id ? 'selected' : ''}}>{{$city->name}}</option>
                                       @endforeach
                               </select>
                               <div class="valid-feedback">
                                 {{ ($errors->has('city_id')) ? $errors->first('city_id') : ''}}
                               </div>
                           </div>
                       </div>


                        <div class="form-group">
                            <label for="name">Thana Name</label>
                            <div class="form-input">
                                <input type="text" class="form-control is-invalid form-control-sm" name="name" id="name" value="{{ $thanas->name}}">
                                <div class="invalid-feedback">
                                  {{ ($errors->has('name')) ? $errors->first('name') : ''}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bangla_name">Thana Bangla Name</label>
                            <div class="form-input">
                                <input type="text" class="form-control is-valid form-control-sm" name="bangla_name" id="bangla_name" value="{{ $thanas->bangla_name? $thanas->bangla_name:'N\A'}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('bangla_name')) ? $errors->first('bangla_name') : ''}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="latitude">Thana Latitude</label>
                            <div class="form-input">
                                <input type="number" class="form-control is-valid form-control-sm input-md" step="0.01" name="latitude" id="latitude" value="{{ $thanas->latitude? $thanas->latitude:'0.00'}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('latitude')) ? $errors->first('latitude') : ''}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="longitude">Thana Longitude</label>
                            <div class="form-input">
                                <input type="number" class="form-control is-valid form-control-sm input-md"step="0.01" name="longitude" id="longitude" value="{{ $thanas->longitude? $thanas->longitude:'0.00'}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('longitude')) ? $errors->first('longitude') : ''}}
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Update</button>
                    </form>
                  </div>
                </div>
              </div>
        <div class="col-sm-4">
          <div id="map">

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
