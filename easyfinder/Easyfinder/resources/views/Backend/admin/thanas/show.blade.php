@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <h4>City Details<a href="{{route('thanas.index')}}" class="btn btn-primary pull-right">Back</a></h4>
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-6">
                   <p>Thana name</p>
                   <p>Thana Bangla Name</p>
                   <p>Thana Latitude </p>
                   <p>Longitude</p>
            </div>
            <div class="col-sm-6">
                   <p>City: {{$thanas->city->name}}, Thana: {{$thanas->name}}</p>
                   <p>{{$thanas->bangla_name?$thanas->bangla_name:'N\A'}} </p>
                   <p>{{$thanas->latitude?$thanas->latitude:'N\A'}} </p>
                   <p>{{$thanas->longitude?$thanas->longitude:'N\A'}} </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div id="map">

            </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
