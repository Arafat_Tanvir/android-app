@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
 		<div class="row ">
 			<div class="col-xs-8 col-sm-8 col-md-8 offset-xs-2 offset-sm-2 offset-md-2">
      <div class="card">
        <div class="card-header">
          <h2>City Create Form <span class="pull-right"><a href="{{ route('wards.index')}}">Back</a></span></h2>
        </div>
        <div class="card-body">
      <form method="POST" action="{{ route('wards.store') }}">
          @csrf
          <div class="form-group">
              <label for="latitude">Thana Name</label>
              <div class="form-input">
                  <select name="thana_id" class="form-control is-valid form-control-sm input-md" value="{{old('thana_id')}}">
                          <option value="0" disabled="true" selected="true">===Select Parent Category for this Category===</option>
                          @foreach($thanas as $thana)
                          <option value="{{$thana->id}}">{{$thana->name}}</option>
                          @endforeach
                  </select>
                  <div class="valid-feedback">
                    {{ ($errors->has('thana_id')) ? $errors->first('thana_id') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group">
              <label for="name">Ward Name</label>
              <div class="form-input">
                  <input type="text" class="form-control is-invalid form-control-sm" name="name" id="name" placeholder="Enter Ward name" value="" required>
                  <div class="invalid-feedback">
                    {{ ($errors->has('name')) ? $errors->first('name') : ''}}
                  </div>
              </div>
          </div>
          <div class="form-group">
              <label for="bangla_name">Ward Bangla Name</label>
              <div class="form-input">
                  <input type="text" class="form-control is-valid form-control-sm" name="bangla_name" id="bangla_name" placeholder="Enter Ward Bangla Name" value="">
                  <div class="valid-feedback">
                    {{ ($errors->has('bangla_name')) ? $errors->first('bangla_name') : ''}}
                  </div>
              </div>
          </div>
          <div class="form-group">
              <label for="latitude">Ward Latitude</label>
              <div class="form-input">
                  <input type="number" class="form-control is-valid form-control-sm input-md" step="0.01" name="latitude" id="latitude" placeholder="Enter Ward latitude" value="">
                  <div class="valid-feedback">
                    {{ ($errors->has('latitude')) ? $errors->first('latitude') : ''}}
                  </div>
              </div>
          </div>
          <div class="form-group">
              <label for="longitude">Ward Longitude</label>
              <div class="form-input">
                  <input type="number" class="form-control is-valid form-control-sm input-md"step="0.01" name="longitude" id="longitude" placeholder="Enter Ward longitude" value="">
                  <div class="valid-feedback">
                    {{ ($errors->has('longitude')) ? $errors->first('longitude') : ''}}
                  </div>
              </div>
          </div>

          <button class="btn btn-primary" type="submit">Create</button>
      </form>
    </div>
  </div>
</div>

</div>
</div>
@endsection
