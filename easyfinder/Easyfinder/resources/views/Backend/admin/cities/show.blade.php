@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <h4>City Details<a href="{{route('cities.index')}}" class="btn btn-primary pull-right">Back</a></h4>
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-6">
                   <p>name</p>
                   <p>Bangla Name</p>
                   <p>Latitude </p>
                   <p>Longitude</p>
            </div>
            <div class="col-sm-6">
                   <p>{{$cities->name}} </p>
                   <p>{{$cities->bangla_name?$cities->bangla_name:'N\A'}} </p>
                   <p>{{$cities->latitude?$cities->latitude:'N\A'}} </p>
                   <p>{{$cities->longitude?$cities->longitude:'N\A'}} </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
              <div id="map">

              </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
