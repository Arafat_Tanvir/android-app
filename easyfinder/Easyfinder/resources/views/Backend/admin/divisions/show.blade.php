@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <h4>Divisions Details<a href="{{route('divisions.index')}}" class="btn btn-primary pull-right">Back</a></h4>
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-4">
                   <p>name</p>
                   <p>Bangla Name</p>
                   <p>Latitude </p>
                   <p>Longitude</p>
            </div>
            <div class="col-sm-8">
                   <p>{{$divisions->name}} </p>
                   <p>{{$divisions->bangla_name?$divisions->bangla_name:'N\A'}} </p>
                   <p>{{$divisions->latitude?$divisions->latitude:'N\A'}} </p>
                   <p>{{$divisions->longitude?$divisions->longitude:'N\A'}} </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div id="map">

  </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
