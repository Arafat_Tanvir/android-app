@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <div class="card-header mt-2">
        <h2>Divisions List<span class="pull-right"><a href="{{ route('divisions.create')}}">Create</a></span></h2>
      </div>
      <div class="table-responsive mt-2">
        <table id="divisions" class="table table-bordered table-striped">
          <caption>List of divisions</caption>
          <thead>
  					<tr>
  						<th>SL</th>
  						<th>Name</th>
  						<th>Bangla name</th>
  						<th>Latitude</th>
              <th>Longitude</th>
              <th>Show Details</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($divisions as $city)
  						<td>{{ $a++ }}</td>
  						<td>{{ $city->name }}</td>
  						<td>
  						    @if($city->bangla_name)
                  <p>{{ $city->bangla_name}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
  						    @if($city->latitude)
                  <p>{{ $city->latitude}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
  						    @if($city->longitude)
                  <p>{{ $city->longitude}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
                <a href="{{route('divisions.show', $city->id)}}" class="badge badge-primary">Show</a>
              </td>
  						<td>
  							<a href="{{route('divisions.edit', $city->id)}}" class="badge badge-warning">Edit</a>
                <a href="#" onclick="return confirm('are you sure')">
									<form class="pull-right form-inline" action="{{route('divisions.destroy', $city->id)}}" method="POST">
										{{csrf_field()}}
										{{method_field('DELETE')}}
										<button class="badge badge-danger" type="submit">Delete</button>
									</form>
								</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
	$(document).ready(function() {
    $('#divisions').DataTable();
} );
</script>
@endsection
