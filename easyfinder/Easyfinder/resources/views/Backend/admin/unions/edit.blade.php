@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <div class="row">
        <div class="col-sm-8">
          <div class="card">
                <div class="card-header">
                  <h2>Union Edit <span class="pull-right"><a href="{{ route('unions.index')}}">Back</a></span></h2>
                </div>
                 <div class="card-body">
                   <form method="POST" action="{{ route('unions.update',$unions->id) }}" >
                       @csrf
                       @method('PUT')

                       <div class="form-group">
                           <label for="latitude">Upazila Name</label>
                           <div class="form-input">
                               <select name="upazila_id" class="form-control is-valid form-control-sm input-md" value="{{old('city_id')}}">
                                       <option value="0" disabled="true" selected="true">===Select Under City===</option>
                                       @foreach($upazilas as $upazila)
                                       <option value="{{$upazila->id}}" {{ $upazila->id == $unions->upazila_id ? 'selected' : ''}}>{{$upazila->name}}</option>
                                       @endforeach
                               </select>
                               <div class="valid-feedback">
                                 {{ ($errors->has('upazila_id')) ? $errors->first('upazila_id') : ''}}
                               </div>
                           </div>
                       </div>


                        <div class="form-group">
                            <label for="name">Union Name</label>
                            <div class="form-input">
                                <input type="text" class="form-control is-invalid form-control-sm" name="name" id="name" value="{{ $unions->name}}">
                                <div class="invalid-feedback">
                                  {{ ($errors->has('name')) ? $errors->first('name') : ''}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bangla_name">Union Bangla Name</label>
                            <div class="form-input">
                                <input type="text" class="form-control is-valid form-control-sm" name="bangla_name" id="bangla_name" value="{{ $unions->bangla_name? $unions->bangla_name:'N\A'}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('bangla_name')) ? $errors->first('bangla_name') : ''}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="latitude">Union Latitude</label>
                            <div class="form-input">
                                <input type="number" class="form-control is-valid form-control-sm input-md" step="0.01" name="latitude" id="latitude" value="{{ $unions->latitude? $unions->latitude:'0.00'}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('latitude')) ? $errors->first('latitude') : ''}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="longitude">Union Longitude</label>
                            <div class="form-input">
                                <input type="number" class="form-control is-valid form-control-sm input-md"step="0.01" name="longitude" id="longitude" value="{{ $unions->longitude? $unions->longitude:'0.00'}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('longitude')) ? $errors->first('longitude') : ''}}
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Update</button>
                    </form>
                  </div>
                </div>
              </div>
        <div class="col-sm-4">
          <div id="map">

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
