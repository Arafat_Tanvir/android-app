@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
      <div class="card-header mt-2">
        <h2>Union List<span class="pull-right"><a href="{{ route('unions.create')}}">Create</a></span></h2>
      </div>
      <div class="table-responsive mt-2">
        <table id="unions" class="table table-bordered table-striped">
          <caption>List of unions</caption>
          <thead>
  					<tr>
  						<th>SL</th>
  						<th>Name</th>
  						<th>Bangla name</th>
  						<th>Latitude</th>
              <th>Longitude</th>
              <th>Parent Union Name</th>
              <th>Show Details</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($unions as $union)
  						<td>{{ $a++ }}</td>
  						<td>{{ $union->name }}</td>
  						<td>
  						    @if($union->bangla_name)
                  <p>{{ $union->bangla_name}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
  						    @if($union->latitude)
                  <p>{{ $union->latitude}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
  						    @if($union->longitude)
                  <p>{{ $union->longitude}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td style="color:green">{{$union->upazila->name}}</td>
              <td>
                <a href="{{route('unions.show', $union->id)}}" class="badge badge-primary">Show</a>
              </td>
  						<td>
  							<a href="{{route('unions.edit', $union->id)}}" class="badge badge-warning">Edit</a>
                <a href="#" onclick="return confirm('are you sure')">
									<form class="pull-right form-inline" action="{{route('unions.destroy', $union->id)}}" method="POST">
										{{csrf_field()}}
										{{method_field('DELETE')}}
										<button class="badge badge-danger" type="submit">Delete</button>
									</form>
								</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
	$(document).ready(function() {
    $('#unions').DataTable();
} );
</script>
@endsection
