<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{asset('css/map.css')}}" >

    <title>Hello, world!</title>
  </head>
  <body>
            @yield('content')
            <p></p>

    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVxQv5x5vPvmC64EI3hEWYc9f2tbbtJXw"></script>
    <script type="text/javascript" src="{{asset('js/map.js')}}">

    </script>
  </body>
</html>
