<?php


 class DbOperations
 {
 	private $con;
 	
 	function __construct()
 	{
 		require_once dirname(__FILE__).'/dbconnector.php';
 		$db=new DbConnect();
 		$this->con=$db->connect();

 	}


 	public function createUser($name,$email,$password,$city,$national_id,$religion,$gender,$mobile,$address){
 		$password=md5($password);
 		$stmt=$this->con->prepare("INSERT INTO 'users'('id','name','email','password','city','national_id','religion','gender','mobile','address')VALUES(NULL,?,?,?,?,?,?,?,?,?);");


 		$stmt->bind_param("ssssssss",$name,$email,$password,$city,$national_id,$religion,$gender,$mobile,$address);
 		if($stmt->execute()){
 			return true;
 		}else{
 			return false;
 		}
 	}
 }

?>