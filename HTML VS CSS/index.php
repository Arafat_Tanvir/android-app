<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Bootstrap 101 Template</title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
	</head>
	<body>
		<div id="all">
			<div id="left">
			</div>
			<div id="right">
				<div id="header">
					<div id="logo">
						
					</div>
					<div id="manu">
						<a href="#"><input type="button" value="Home" id="submit"></input></a>
						<a href="#"><input type="button" value="Contact"id="submit"></input></a>
						<a href="#"><input type="button" value="HelpLine" id="submit"></input></a>
					</div>
					
				</div>
				<div id="content">
					<div id="root">
						<div id="main_content">
							
						</div>
						<div id="footer">
							<div id="facebook">
								
							</div>
							<div id="banner">
								
							</div>
						</div>
					</div>
					<div id="link">
						
					</div>
				</div>
			</div>
			
		</div>
		<script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>