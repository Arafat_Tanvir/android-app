<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Student Information</title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
	</head>
	<body>
		<div class="table col-sm-12">
			<table class ="table table-striped">
				<caption style="text-align: center;color: red;">List of Student</caption>
				<thead class="table table-dark">
					<tr>
						<th>Serial Number</th>
						<th>Student Name</th>
						<th>Email</th>
						<th>Student Roll</th>
						<th>School</th>
					</tr>
				</thead>
				<tr>
					<td>1</td>
					<td>Fatema Begum</td>
					<td>fatema@gmail.com</td>
					<td>1203</td>
					<td>Gvt .Women college Chittagong</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Fatema Begum</td>
					<td>fatema@gmail.com</td>
					<td>1203</td>
					<td>Gvt .Women college Chittagong</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Fatema Begum</td>
					<td>fatema@gmail.com</td>
					<td>1203</td>
					<td>Gvt .Women college Chittagong</td>
				</tr>
				<tr>
					<td>4</td>
					<td>Fatema Begum</td>
					<td>fatema@gmail.com</td>
					<td>1203</td>
					<td>Gvt .Women college Chittagong</td>
				</tr>
			</table>
		</div>
		<div class="link">
			<head>Home Link </head>
			<a href="index.php">Home</a>
		</div>
		<div class="unorderlist">
			<head>Unorder List </head>
			<ul>
				<li>Coffee</li>
				<li>Tea</li>
				<li>Milk</li>
			</ul>
		</div>
		<div class="orderlist">
			<head>Order List </head>
			<ol>
				<li>Coffee</li>
				<li>Tea</li>
				<li>Milk</li>
			</ol>
		</div>
		
	</body>
</html>