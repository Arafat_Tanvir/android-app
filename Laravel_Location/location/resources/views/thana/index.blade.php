@extends('layouts.app')

@section('content')
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
 	<div class="container well">
 		<div class="row">
 			<div class="col-sm-8 col-sm-offset-2">
 				<h1>Station List<a href="{{route('location.create')}}" class="btn btn-primary pull-right">Create</a></h1>
 				@if(Session::has('status'))
				<div class="alert alert-success">
					{{Session('status')}}
				</div>
				@endif
 				<table class="table table-bordered" id="location">
 					<thead>
 						<tr>
 							<th>SL</th>
 							<th>City</th>
 							<th width="180">Action</th>
 						</tr>
 					</thead>
 					<tbody>
 						<div style="display: none;">{{$i=1}}</div>
 						@foreach($locations as $location)
 						<tr>
 							<td>{{ $i++ }}</td>
 							<td>{{ $location->city }}</td>
 							
 							<td>
 								<a href="{{route('location.show', $location->id)}}" class="btn btn-warning btn-sm">Show</a>
								<a href="{{route('location.edit', $location->id)}}" class="btn btn-warning btn-sm">Edit</a>

								<a href="#" onclick="return confirm('are you sure')">
								<form class="pull-right" action="{{route('location.destroy', $location->id)}}" method="POST">
									{{csrf_field()}}
									{{method_field('DELETE')}}
									<button class="btn btn-danger btn-sm" type="submit">Delete</button>
									
								</form>

								</a>
 							</td>
 						</tr>
 						@endforeach
 					</tbody>
 				</table>
 			</div>
 		</div>
 	</div> 
@endsection
