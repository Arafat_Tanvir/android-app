@extends('layouts.app')
@section('content')
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<div class="location">
	<div class="container well">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Create City<a href="{{route('location.index')}}" class="btn btn-primary pull-right">back</a></h1>
				<form action="{{route('location.store')}}" enctype="multipart/form-data" method="POST">
					{{csrf_field()}}
					<div class="form-group">
						<label for="thana">Thana</label>
						<input type="text" id="thana" name="thana" class="form-control" placeholder="Enter thana">
					</div>
					<button type="submit" class="btn btn-success">Create</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection