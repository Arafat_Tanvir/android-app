@extends('layouts.app')

@section('content')
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
   <div class="location">
 	<div class="container well">
 		<div class="row">
 			<div class="col-sm-8 col-sm-offset-2">

 				<h1>Create City<a href="{{route('location.index')}}" class="btn btn-primary pull-right">back</a></h1>
 				<form action="{{route('location.store')}}" enctype="multipart/form-data" method="POST">
 					{{csrf_field()}}

 					<div class="form-group">
 						<label for="city">City</label>
 						<input type="text" id="city" name="city" class="form-control" placeholder="Enter City">
 					</div>

 					<button type="submit" class="btn btn-success">Create</button>
 				</form>
 			</div>
 		</div>
 	</div>
 </div>
@endsection