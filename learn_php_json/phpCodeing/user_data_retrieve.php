<?php
require_once '../DatabaseConnection/DbOperation.php';
 
 $response=array();

 if ($_SERVER['REQUEST_METHOD']=='POST') {
 	if(@$_POST['email'] and @$_POST['password']){
 		$db=new DbOperation();
 		if($db->userLogin($_POST['email'],$_POST['password'])){
 			$user=$db->get_thana($_POST['thana']);
 			$response['error']=false;
 			$response['date_from']=$user['date_from'];
 			$response['religion']=$user['religion'];
 			$response['address']=$user['address'];
 		}else{
 			$response['error']=true;
 		$response['message']="Invalid user and password";
 		}

 	}else{
 		$response['error']=true;
 		$response['message']="Required fields are missing";
 	}

 }
echo json_encode($response);
?>