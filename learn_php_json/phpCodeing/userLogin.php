<?php
require_once '../DatabaseConnection/DbOperation.php';
 
 $response=array();

 if ($_SERVER['REQUEST_METHOD']=='POST') {
 	if(@$_POST['email'] and @$_POST['password']){
 		$db=new DbOperation();
 		if($db->userLogin($_POST['email'],$_POST['password'])){
 			$user=$db->getUserByEmail($_POST['email']);
 			$response['error']=false;
 			$response['id']=$user['id'];
 			$response['name']=$user['name'];
 			$response['email']=$user['email'];
 			$response['password']=$user['password'];
 			$response['city']=$user['city'];
 			$response['national_id']=$user['national_id'];
 			$response['religion']=$user['religion'];
 			$response['gender']=$user['gender'];
 			$response['mobile']=$user['mobile'];
 			$response['address']=$user['address'];
 		}else{
 			$response['error']=true;
 		$response['message']="Invalid user and password";
 		}

 	}else{
 		$response['error']=true;
 		$response['message']="Required fields are missing";
 	}

 }
echo json_encode($response);
?>