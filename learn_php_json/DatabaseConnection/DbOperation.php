<?php
 /**
  * 
  */
 class DbOperation
 {
 	
 	function __construct()
 	{
 		require_once dirname(__FILE__).'/Dbconnection.php';
 		$db=new DbConnect();
 		$this->con=$db->connect();
 	}
 	//CRUD->C->Create
 	public function createUser($name,$email,$password,$city,$national_id,$religion,$gender,$mobile,$address){
 		if($this->isUserExist($email,$password)){
 			return 0;
 		}else{
 		$password=md5($password);
 		$stmt=$this->con->prepare("INSERT INTO `users` (`id`, `name`, `email`, `password`, `city`, `national_id`, `religion`, `gender`, `mobile`, `address`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
 		$stmt->bind_param("sssssssss",$name,$email,$password,$city,$national_id,$religion,$gender,$mobile,$address);
 		if ($stmt->execute()) {
 			return 1;
 		}else{
 			return 2;
 		}
 	  }
 	}
 	public function userLogin($email,$password){
 		$password=md5($password);
 		$stmt=$this->con->prepare("SELECT id FROM `users` WHERE email= ? AND password = ? ");
 		$stmt->bind_param("ss",$email,$password);
 		$stmt->execute();
 		$stmt->store_result();
 		return $stmt->num_rows >0;
 	}

 	public function getUserByEmail($email){
 		$stmt=$this->con->prepare("SELECT * FROM `users` WHERE email = ?");
 		$stmt->bind_param("s",$email);
 		$stmt->execute();
 		return $stmt->get_result()->fetch_assoc();

 	}


 	private function isUserExist($email,$password){
 		$stmt=$this->con->prepare("SELECT id FROM `users` WHERE email= ? OR password= ?");
 		$stmt->bind_param("ss",$email,$password);
 		$stmt->execute();
 		$stmt->store_result();
 		return $stmt->num_rows > 0;
 	}
 	public function room_data(){

 		$stmt=$this->con->prepare("SELECT id, name,email,city,national_id,religion,gender,mobile,address FROM users;");
 		$stmt->bind_result($id, $name,$email,$city,$national_id,$religion,$gender,$mobile,$address);
 		$stmt->execute();
 		return $stmt->num_rows > 0;
		
 		
 	}

 	public function post_room_data(){
 		$stmt=$this->con->prepare("SELECT * FROM room_post");
 		$stmt->execute();
 		return $stmt->num_rows > 0;

 	}
 	public function upload($file){
        //$extension = $file->getClientOrginalExtension();
        $name =time().$file->getClientOriginalName();
        $filename=$name;
        $current = public_path('image/');
        $file->move($current, $filename);
        return 'image/'.$filename;
    }
 }



?>